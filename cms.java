import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

public class cms{
    WebDriver driver = new HtmlUnitDriver();


    @Test @Parameters(value = {"CMS_HOME", "LOGIN", "PASSWORD", "TIMEOUT_CUSTOM"}) public void login(String CMS_HOME, String LOGIN, String PASSWORD, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << login >> test");
        driver.get(CMS_HOME);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.titleContains("Campaign Management System"));
        Assert.assertEquals(driver.getTitle(), "Campaign Management System");
        WebElement email = driver.findElement(By.id("email"));
        WebElement password = driver.findElement(By.id("password"));
        email.sendKeys(LOGIN);
        Assert.assertNotNull(email);
        password.sendKeys(PASSWORD);
        Assert.assertNotNull(password);
        password.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(LOGIN)));
        System.out.println("End of << login >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "USER_ID", "TIMEOUT_CUSTOM"}) public void loadUserGoalsPage(String CMS_HOME, String USER_ID, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadUserGoalsPage >> test");
        driver.get(CMS_HOME + "/goals/user");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("userId")));
        WebElement user_id = driver.findElement(By.id("userId"));
        user_id.sendKeys(USER_ID);
        user_id.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("widget")));
        System.out.println("End of << loadUserGoalsPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadSettingsPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadSettingsPage >> test");
        driver.get(CMS_HOME + "/settings/page");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Settings");
        driver.findElement(By.id("setting"));
        System.out.println("End of << loadSettingsPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadCampaignsPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadCampaignsPage >> test");
        driver.get(CMS_HOME + "/campaigns/1");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Campaigns");
        driver.findElement(By.id("today"));
        System.out.println("End of << loadCampaignsPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadTGsPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadTGsPage >> test");
        driver.get(CMS_HOME + "/target_groups/list");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Target Groups:");
        driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/section/form/div/input[1]"));
        System.out.println("End of << loadTGsPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadTemplatesPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadTemplatesPage >> test");
        driver.get(CMS_HOME + "/templates");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Templates");
        driver.findElement(By.id("table-dynamic"));
        System.out.println("End of << loadTemplatesPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadGoalsPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadGoalsPage >> test");
        driver.get(CMS_HOME + "/offers/goals/list");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Goals");
        driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/section/form/div/input[1]"));
        System.out.println("End of << loadGoalsPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadBonusesPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadBonusesPage >> test");
        driver.get(CMS_HOME + "/offers/bonuses/list");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Bonuses");
        driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/section/form/div/input[1]"));
        System.out.println("End of << loadBonusesPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadFunnelPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadFunnelPage >> test");
        driver.get(CMS_HOME + "/reports/funnels");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Funnel");
        driver.findElement(By.className("col-md-6"));
        System.out.println("End of << loadFunnelPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadLinksPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadLinksPage >> test");
        driver.get(CMS_HOME + "/linkbuilder/list");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Link Builder");
        driver.findElement(By.className("widget"));
        System.out.println("End of << loadLinksPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadConfigurationPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadConfigurationPage >> test");
        driver.get(CMS_HOME + "/config");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Configuration");
        driver.findElement(By.id("first-deposit-selection"));
        System.out.println("End of << loadConfigurationPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void loadReportsPage(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << loadReportsPage >> test");
        driver.get(CMS_HOME + "/report");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(),"Reports");
        System.out.println("End of << loadReportsPage >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "CAMPAIGN_NAME", "TIMEOUT_CUSTOM"}) public void startCampaign(String CMS_HOME, String CAMPAIGN_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << startCampaign >> test");
        driver.get(CMS_HOME + "/campaigns/start?campaignId=" + CAMPAIGN_NAME);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(), "Campaigns");
        System.out.println("End of << startCampaign >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "USER_ID", "GOAL_NAME", "TIMEOUT_CUSTOM"}) public void addGoalToUser(String CMS_HOME, String USER_ID, String GOAL_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << addGoalToUser >> test");
        driver.get(CMS_HOME + "/goals/add?userId=" + USER_ID);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("goal_id")));
        WebElement select = driver.findElement(By.id("goal_id"));
        Select option = new Select(select);
        option.selectByVisibleText(GOAL_NAME);
        select.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertTrue(driver.getPageSource().contains(GOAL_NAME));
        System.out.println("End of << addGoalToUser >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "USER_ID", "BONUS_NAME", "TIMEOUT_CUSTOM"}) public void addBonusToUser(String CMS_HOME, String USER_ID, String BONUS_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << addBonusToUser >> test");
        driver.get(CMS_HOME + "/goals/add_bonus?userId=" + USER_ID);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("bonus_id")));
        WebElement select = driver.findElement(By.id("bonus_id"));
        Select option = new Select(select);
        option.selectByVisibleText(BONUS_NAME);
        select.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertTrue(driver.getPageSource().contains(BONUS_NAME));
        System.out.println("End of << addBonusToUser >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "USER_ID", "GOAL_NAME", "TIMEOUT_CUSTOM"}) public void removeGoalFromUser(String CMS_HOME, String USER_ID, String GOAL_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << removeGoalFromUser >> test");
        driver.get(CMS_HOME + "/goals/delete?user_id=" + USER_ID + "&goal_id=" + GOAL_NAME);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        System.out.println("End of << removeGoalFromUser >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "USER_ID", "BONUS_NAME", "TIMEOUT_CUSTOM"}) public void removeBonusFromUser(String CMS_HOME, String USER_ID, String BONUS_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << removeBonusFromUser >> test");
        driver.get(CMS_HOME + "/goals/delete_bonus?user_id=" + USER_ID + "&goal_id=" + BONUS_NAME);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        System.out.println("End of << removeBonusFromUser >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "CAMPAIGN_NAME", "TIMEOUT_CUSTOM"}) public void createCampaign(String CMS_HOME, String CAMPAIGN_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << createCampaign >> test");
        driver.get(CMS_HOME + "/campaigns/create");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("page-title")));
        Assert.assertEquals(driver.findElement(By.className("page-title")).getText(), "Edit Campaign draft campaign");
        WebElement name = driver.findElement(By.id("_id"));
        WebElement duration = driver.findElement(By.id("duration"));
        WebElement end_time = driver.findElement(By.id("end_time"));
        name.sendKeys(CAMPAIGN_NAME);
        duration.sendKeys("001h:15m");
        end_time.sendKeys("22-08-2020 17:31");
        name.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("page-title")));
        Assert.assertEquals(driver.findElement(By.className("page-title")).getText(), "Edit Campaign " + CAMPAIGN_NAME);
        System.out.println("End of << createCampaign >> test");
    }

    @Test @Parameters (value = {"CMS_HOME","GOAL_NAME", "BONUS_NAME", "TIMEOUT_CUSTOM"}) public void createGoal(String CMS_HOME,String GOAL_NAME, String BONUS_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << createGoal >> test");
        driver.get(CMS_HOME + "/offers/goals/new");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id")));
        WebElement id = driver.findElement(By.id("id"));
        WebElement state = driver.findElement(By.id("state"));
        WebElement link = driver.findElement(By.id("link"));
        WebElement image = driver.findElement(By.id("image"));
        WebElement template = driver.findElement(By.id("template"));
        WebElement goal_settings_notification_interval =driver.findElement(By.id("goal_settings_notification_interval"));
        WebElement goal_settings_duration = driver.findElement(By.id("goal_settings_duration"));
        WebElement precondition_type = driver.findElement(By.id("precondition_type"));
        WebElement precondition_price_spec_operation = driver.findElement(By.id("precondition_price_spec_operation"));
        WebElement b_id = driver.findElement(By.id("bonus"));
        WebElement add_bonus_button = driver.findElement(By.id("add_bonus"));
        WebElement save_goal_button = driver.findElement(By.id("save-goal-btn"));

        id.sendKeys(GOAL_NAME);

        Select optionState = new Select(state);
        optionState.selectByVisibleText("Default");

        Select optionLink = new Select(link);
        optionLink.selectByVisibleText("byu chips");

        Select optionImg = new Select(image);
        optionImg.selectByVisibleText("Cup");

        Select optionTemplate = new Select(template);
        optionTemplate.selectByVisibleText("main_default");

        goal_settings_notification_interval.sendKeys("10:00");
        goal_settings_duration.sendKeys("00:10");

        Select optionPreconditionType = new Select(precondition_type);
        optionPreconditionType.selectByVisibleText("IN_APP");

        Select optionPreconditionPrice = new Select(precondition_price_spec_operation);
        optionPreconditionPrice.selectByVisibleText("Any");

        Select optionBonus = new Select(b_id);
        optionBonus.selectByVisibleText(BONUS_NAME);

        add_bonus_button.click();

        save_goal_button.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[2]/div[3]/section/table/tbody/tr[1]/td[1]")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[3]/section/table/tbody/tr[1]/td[1]")).getText(), GOAL_NAME);
        System.out.println("End of << createGoal >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "BONUS_NAME", "TIMEOUT_CUSTOM"})public void createBonus(String CMS_HOME, String BONUS_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << createBonus >> test");
        driver.get(CMS_HOME + "/offers/bonuses/new");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id")));
        WebElement id = driver.findElement(By.id("id"));
        WebElement value = driver.findElement(By.id("value"));
        WebElement select = driver.findElement(By.id("category"));
        id.sendKeys(BONUS_NAME);
        value.sendKeys("100");
        Select option = new Select(select);
        option.selectByVisibleText("IN_APP_PERCENT");
        select.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(), "Bonuses");
        System.out.println("End of << createBonus >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "CAMPAIGN_NAME", "TIMEOUT_CUSTOM"}) public void deleteCampaign(String CMS_HOME, String CAMPAIGN_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << deleteCampaign >> test");
        driver.get(CMS_HOME + "/campaigns/remove?campaignId=" + CAMPAIGN_NAME);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(), "Campaigns");
        driver.get(CMS_HOME + "/campaigns/edit?campaignId=" + CAMPAIGN_NAME);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("_id")));
        Assert.assertEquals(driver.findElement(By.id("_id")).getText(), "");
        System.out.println("End of << deleteCampaign >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "GOAL_NAME", "TIMEOUT_CUSTOM"}) public void deleteGoal(String CMS_HOME, String GOAL_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << deleteGoal >> test");
        driver.get(CMS_HOME + "/offers/goals/remove/" + GOAL_NAME);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(), "Goals");
        WebElement search = driver.findElement(By.name("query"));
        search.sendKeys(GOAL_NAME);
        search.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[2]/div[2]/section/div/em")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/section/div/em")).getText(), "Nothing to display");
        System.out.println("End of << deleteGoal >> test");
    }

    @Test @Parameters (value = {"CMS_HOME", "BONUS_NAME", "TIMEOUT_CUSTOM"}) public void deleteBonus(String CMS_HOME, String BONUS_NAME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << deleteBonus >> test");
        driver.get(CMS_HOME + "/offers/bonuses/remove/" + BONUS_NAME);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div/h2")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div/h2")).getText(), "Bonuses");
        WebElement search = driver.findElement(By.name("query"));
        search.sendKeys(BONUS_NAME);
        search.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[2]/div[2]/section/div/em")));
        Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/section/div/em")).getText(), "Nothing to display");
        System.out.println("End of << deleteBonus >> test");
    }

}