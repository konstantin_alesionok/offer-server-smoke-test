import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

public class clarity {
    WebDriver driver = new HtmlUnitDriver();

    @Test @Parameters(value = {"CMS_HOME", "LOGIN", "PASSWORD", "TIMEOUT_CUSTOM"}) public void login(String CMS_HOME, String LOGIN, String PASSWORD, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << login >> test");
        driver.get(CMS_HOME);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("form-signin-heading")));
        Assert.assertEquals(driver.findElement(By.className("form-signin-heading")).getText(), "Login to your account");
        WebElement email = driver.findElement(By.id("email"));
        WebElement password = driver.findElement(By.id("password"));
        email.sendKeys(LOGIN);
        Assert.assertNotNull(email);
        password.sendKeys(PASSWORD);
        Assert.assertNotNull(password);
        password.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(LOGIN)));
        driver.findElement(By.linkText(LOGIN));
        System.out.println("End of << login >> test");
    }

    @Test @Parameters(value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void goDashboard(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << goDashboard >> test");
        driver.get(CMS_HOME);
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("side-nav")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement sideNav = driver.findElement(By.id("side-nav"));
        System.out.println(sideNav);
        System.out.println("End of << goDashboard >> test");
    }

    @Test @Parameters(value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void goFunnel(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << goFunnel >> test");
        driver.get(CMS_HOME + "/funnels");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[1]/h2"));
        System.out.println(header.getText());
        System.out.println("End of << goFunnel >> test");
    }

    @Test @Parameters(value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void goReports(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << goReports >> test");
        driver.get(CMS_HOME + "/report/kpi_report");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/iframe")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement iframe1 = driver.findElement(By.xpath("/html/body/div[2]/div/iframe"));
        System.out.println(iframe1);
        driver.get(CMS_HOME + "/report/cross_app_conversion");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/iframe")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement iframe2 = driver.findElement(By.xpath("/html/body/div[2]/div/iframe"));
        System.out.println(iframe2);
        System.out.println("End of << goReports >> test");
    }

    @Test @Parameters(value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void goCampaigns(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << goCampaigns >> test");
        driver.get(CMS_HOME + "/campaigns/calendar");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("page-title")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header = driver.findElement(By.className("page-title"));
        System.out.println(header.getText());
        WebElement dayBtn = driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div[1]/section/header/div/div/label[3]/input"));
        WebElement weekBtn = driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div[1]/section/header/div/div/label[2]/input"));
        WebElement monthBtn = driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div[1]/section/header/div/div/label[1]/input"));
        System.out.println("End of << goCampaigns >> test");
    }

    @Test @Parameters(value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void goSegments(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << goSegments >> test");
        driver.get(CMS_HOME + "/segments/list");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/h2"));
        WebElement selector = driver.findElement(By.xpath("/html/body/div[2]/div/section[1]/div[1]/div[2]/div/select"));
        System.out.println(header.getText());
        System.out.println("End of << goSegments >> test");
    }

    @Test @Parameters(value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void goAppUsers(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << goAppUsers >> test");
        driver.get(CMS_HOME + "/users/list");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/h2"));
        WebElement selector = driver.findElement(By.xpath("/html/body/div[2]/div/section/form[1]/div/div/section/dl/dd/select"));
        System.out.println(header.getText());
        System.out.println("End of << goAppUsers >> test");
    }

    @Test @Parameters(value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void goSettings(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << goSettings >> test");
        driver.get(CMS_HOME + "/settings/page");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div[1]/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[1]/h2"));
        WebElement search = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/section/div/div[2]/form/div/input[1]"));
        System.out.println(header.getText());
        System.out.println("End of << goSettings >> test");
    }

    @Test @Parameters(value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void goPushes(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << goPushes >> test");
        driver.get(CMS_HOME + "/pushes/list");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/h2"));
        System.out.println(header.getText());
        System.out.println("End of << goPushes >> test");
    }

    @Test @Parameters(value = {"CMS_HOME", "TIMEOUT_CUSTOM"}) public void goConfiguration(String CMS_HOME, int TIMEOUT_CUSTOM) throws Exception {
        System.out.println("Start of << goConfiguration >> test");
            driver.get(CMS_HOME + "/accounts/list");
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_CUSTOM);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header0 = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/h2"));
        WebElement acc = driver.findElement(By.xpath("/html/body/div[2]/div/section[2]/div/table/tbody/tr[3]/td[1]/a"));
        System.out.println(header0.getText());
            driver.get(CMS_HOME + "/cmsusers/list");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header1 = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/h2"));
        WebElement email = driver.findElement(By.xpath("/html/body/div[2]/div/section[2]/div/table/tbody/tr[1]/td[1]/a"));
        System.out.println(header1.getText());
            driver.get(CMS_HOME + "/apps/list");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header2 = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/h2"));
        WebElement app = driver.findElement(By.className("col-md-12"));
        System.out.println(header2.getText());
            driver.get(CMS_HOME + "/appevents/list");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header3 = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/h2"));
        WebElement event = driver.findElement(By.className("col-md-12"));
        System.out.println(header3.getText());
            driver.get(CMS_HOME + "/currencies/list");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[2]/div/div/div[1]/h2")));
        System.out.println("Page url: " + driver.getCurrentUrl());
        WebElement header4 = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/h2"));
        WebElement btn = driver.findElement(By.xpath("/html/body/div[2]/div/section[1]/div/div/a"));
        System.out.println(header4.getText());
        System.out.println("End of << goConfiguration >> test");
    }
}