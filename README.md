### What is this repository for? ###

* This repository provides console-mode smoke tests for Offer Server
* Version 0.1

### How do I get set up? ###

* ".sh" file will launch test suite. It will compile java file with class-path libraries and run it thru testNG framework.
* Java file contains tests. XML file contains parameters for test suite. Bash file needed to run tests from console.
* /libs contains all necessary selenium libs

### Contribution guidelines ###

* Tests are written by contributors

### Who do I talk to? ###

* Repo owner is Konstantin Alesionok